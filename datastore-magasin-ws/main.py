# -*- coding: utf-8 -*-

# Auteurs:
#    Gabriel Morin et Gia Hoi Nguyen

import webapp2
import logging
import traceback
import time
import datetime
import json

from google.appengine.ext import ndb
from google.appengine.ext import db

from models import *


def serialiser_pour_json(objet):
    """ Permet de sérialiser les dates et heures pour transformer
        un objet en JSON.

        Args:
            objet (obj): L'objet à sérialiser.

        Returns:
            obj : Si c'est une date et heure, retourne une version sérialisée
                  selon le format ISO (str); autrement, retourne l'objet
                  original non modifié.
    """
    if isinstance(objet, datetime.datetime):
        # Pour une date et heure, on retourne une chaîne
        # au format ISO (sans les millisecondes).
        return objet.replace(microsecond=0).isoformat()
    elif isinstance(objet, datetime.date):
        # Pour une date, on retourne une chaîne au format ISO.
        return objet.isoformat()
    else:
        # Lève une exception si ce n'est pas une date.
        raise TypeError()


def is_json(data):
    if(data[:1] == '{'):
        return True
    else:
        return False


class MainPageHandler(webapp2.RequestHandler):

    def get(self):
        # Permet de vérifier si le service Web est en fonction.
        # On pourrait utiliser cette page pour afficher de l'information
        # (au format HTML) sur le service Web REST.
        self.response.headers['Content-Type'] = 'text/plain; charset=utf-8'
        self.response.out.write('Démo "Service Web REST avec' +
                                ' Google App Engine" en fonction !!!')


class ItemHandler(webapp2.RequestHandler):
    # seule façon de créer un item
    def put(self, id_item, prix=None):
        """ Permet d'ajouter ou modifier un item ayant un certain id.

            Args:
                id_item (str): Le id de l'item à ajouter (path parameter):
                           obligatoire.
                prix (str)   : Le prix de l'item en cas de modification
                            (optionnel)
        """
        try:
            # Création de la clé de l'item
            cle = ndb.Key("Item", id_item)

            # Permet de récupérer l'entité (un objet "Item") liée à la clé;
            # on obtient "None" si aucune entité n'est liée à cette clé.
            item = cle.get()

            # Y a-t-il une entité liée à la clé ?
            if item is None:
                # L'entité n'existe, elle sera créée (201 Created).
                status = 201
                # Création de l'entité item avec une clé spécifiée et,
                # par conséquent, avec l'identifiant associé à cette clé.
                item = Item(key=cle)

                # Remplissage d'un dictionnaire avec les valeurs de la requête
                item_dict_in = json.loads(self.request.body)

                # Récupération du nom
                item.nom = item_dict_in['nom'].strip().title()

                # Récupération de la description
                item.description = item_dict_in['description']

                # Récupération du prix
                item.prix = item_dict_in['prix']

                # Création d'une clé pour la catégorie
                nomCategorie = item_dict_in['categorie']
                cle = ndb.Key("Categorie", nomCategorie)

                # Vérification que la catégorie est valide
                categorie = cle.get()
                if(categorie is not None):
                    # Catégorie valide
                    # Récupération de la catégorie
                    item.categorie = cle
                else:
                    # Catégorie non existante.
                    self.error(400)
                    self.response.write("Catégorie non-existante")
                    self.response.write(nomCategorie)
                    # Fin de l'exécution.
            else:
                # Le corps de la requête doit contenir une expression JSON
                # avec les données de l'item à créer ou à modifier.
                # dictionnaire entrant avant l'écriture de la bd
                requete = self.request.body
                # Si le corps de la requête ne contient pas de JSON
                if(is_json(requete) is False):
                    # On reçoit alors un prix
                    prix = self.request.get('prix').strip().lower()

                    # Si la valeur est un nombre, on modifie le prix de l'item
                    try:
                        item.prix = float(prix)
                        status = 200

                    # La valeur n'est pas un float
                    except (db.BadValueError, ValueError, KeyError):
                        logging.error('%s', traceback.format_exc())
                        # Bad Request.
                        self.error(400)

                else:
                    # Récupération des données JSON
                    item_dict_in = json.loads(self.request.body)

                    # Formtatage du nom
                    item.nom = item_dict_in['nom'].strip().title()

                    # Récupération de la description
                    item.description = item_dict_in['description']

                    # Récupération de la categorie
                    categorie = Categorie()
                    categorie.nom = item_dict_in['categorie']
                    cle = ndb.Key("Categorie", categorie.nom)
                    item.categorie = cle

                    # Récupération du prix
                    item.prix = item_dict_in['prix']
                    status = 200

            # Création ou modification de l'entité "Item"
            # dans le Datastore.
            item.put()

            self.response.set_status(status)

            # On crée un dictionnaire contenant les valeurs de l'objet
            # Pour la représentation en JSON
            item_dict_out = item.to_dict()

            # Formatage du nom
            item_dict_out['nom'] = item.key.id()
            item_dict_out['nom'] = item_dict_out['nom'].strip().title()

            # Formatage de la catégorie
            cleCategorie = item_dict_out['categorie'].id()
            cle = ndb.Key("Categorie", cleCategorie)
            item_dict_out['categorie'] = cle.id().strip().lower()

            item_json_out = json.dumps(item_dict_out,
                                       default=serialiser_pour_json)

            # Configuration de l'entête HTTP "Content-Type" de la réponse.
            self.response.headers['Content-Type'] = ('application/json;' +
                                                     ' charset=utf-8')
            # Le corps de la réponse contiendra une représentation en JSON
            # de l'item qui vient d'être créée ou modifiée.
            self.response.out.write(item_json_out)

        # Exceptions en lien avec les données fournies (liées à la requête).
        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            # Bad Request.
            self.error(400)

        # Exceptions graves lors de l'exécution du code
        # (non liées à la requête).
        except Exception:
            logging.error('%s', traceback.format_exc())
            # Internal Server Error.
            self.error(500)

    def get(self, id_item=None, magasins=None):
            """ Permet d'obtenir une représentation d'un item ayant un certain
                id_item ou bien de tous les items si l'id_item n'est pas
                spécifié.

                Args:
                    id_item (str): Le id d'un item ou bien "None"
                    pour obtenir tous les items  (path parameter):
                    optionnel.

                    prix (str): le prix dans le cas où on voudrait utiliser
                    un filtre dans notre recherche pour mettre une valeur
                    maximale(optionnel)
            """
            try:
                # Est-ce que l'id est présent ?
                if id_item is not None:
                    # Un seul item.
                    cle = ndb.Key('Item', id_item)

                    # Récupération de l'item selon sa clé.
                    item = cle.get()

                    # Est-ce qu'il y a un item relié au id_item?
                    if (item is None):
                        # Not Found.
                        self.error(404)
                        # Fin de l'exécution.
                        return

                    # Début GetMagasinsItem
                    if(magasins is None):

                        # Création d'un dictionnaire contenant les valeurs
                        # de l'item
                        item_dict = item.to_dict()

                        # Formatage du nom
                        item_dict['nom'] = item.key.id()
                        item_dict['nom'] = item_dict['nom'].strip().title()

                        # Récupération de la clé de la catégorie
                        cleCategorie = item_dict['categorie'].id()
                        cle = ndb.Key("Categorie", cleCategorie)

                        # Insertion dans le dictionnaire de la catégorie
                        # sous forme de chaine de caractère en allant chercher
                        # l'id de la clé
                        item_dict['categorie'] = cle.id()

                        # Sérialisation du dictionnaire en JSON
                        json_data = json.dumps(item_dict,
                                               default=serialiser_pour_json)
                        # Statut 200 (OK)
                        self.response.set_status(200)
                    else:
                        if(magasins == 'magasins'):
                            # Création d'une liste d'ItemStock
                            lst_dict_itemStock = []

                            # Requête sur les ItemStock dans le datastore
                            requete = ItemStock.query()
                            # Ajout d'un filtre pour spécifier qu'on
                            # l'item correspondant à celui entré dans l'URL
                            requete = requete.filter(ItemStock.item == cle)

                            # Si la requête renvoie quelque chose
                            if (requete.count() > 0):
                                # Boucle sur les enregistrements de la requête
                                for itemStock in requete.fetch():
                                    # Création d'un dictionnaire contenant les
                                    # valeurs de l'item
                                    itemStock_dict = itemStock.to_dict()

                                    # Formatage du nom du magasin
                                    nom = str(itemStock.key.id())
                                    itemStock_dict['magasin'] = nom

                                    # Formatage du nom de l'item
                                    nomItem = item.nom.strip().lower()
                                    itemStock_dict['item'] = nomItem

                                    # Ajout de l'item à la liste
                                    lst_dict_itemStock.append(itemStock_dict)

                                # Conversion en JSON de la liste
                                json_data = json.dumps(
                                            lst_dict_itemStock,
                                            default=serialiser_pour_json)
                            else:
                                self.error(404)
                                self.response.write(
                                     'Aucun magasin associé à cet article')
                                return
                        else:
                            # La valeur n'est pas égale à "items"
                            # Bad Request.
                            self.error(400)
                            self.response.write("La valeur n'est pas valide")
                            return
                else:
                    # aucun id_item de fourni
                    # Tous les items.
                    lst_dict_item = []
                    # Création d'une requête sur le "Datastore"
                    # (tous les items).
                    requete = Item.query()

                    # Vérification si une valeur a été entrée pour le prix-max
                    prix = self.request.get('prix-max').strip().lower()
                    if (prix != ''):
                        # Ajout d'un filtre pour "prix-max".
                        requete = requete.filter(Item.prix <= float(prix))

                    # Parcours des items retournées par la requête.
                    for item in requete.fetch():
                        # Création d'un dictionnaire contenant les valeurs
                        # de l'item
                        item_dict = item.to_dict()

                        # Formatage du nom
                        item_dict['nom'] = item.key.id()
                        item_dict['nom'] = item_dict['nom'].strip().title()

                        # Récupération de la clé de la catégorie
                        cleCategorie = item_dict['categorie'].id()
                        cle = ndb.Key("Categorie", cleCategorie)

                        # Insertion dans le dictionnaire de la catégorie
                        # sous forme de chaine de caractère en allant chercher
                        # l'id de la clé
                        item_dict['categorie'] = cle.id()
                        lst_dict_item.append(item_dict)

                    # Tri de la liste de par le prix des items
                    lst_dict_item = sorted(lst_dict_item, key=lambda x:
                                           x['prix'], reverse=True)
                    json_data = json.dumps(lst_dict_item,
                                           default=serialiser_pour_json)

                    # Si aucun item n'a été retourné
                    if len(lst_dict_item) == 0:
                        self.response.set_status(204)
                    else:
                        self.response.set_status(200)
                self.response.headers['Content-Type'] = ('application/json;' +
                                                         ' charset=utf-8')
                self.response.out.write(json_data)

            except (db.BadValueError, ValueError, KeyError):
                logging.error('%s', traceback.format_exc())
                self.error(400)

            except Exception:
                logging.error('%s', traceback.format_exc())
                self.error(500)

    def delete(self, id_item=None):
            """ Permet de supprimer un item ayant un certain id
                ou bien toutes les item si l'id n'est pas spécifié.

                Args:
                    id_item (str): l'id d'un certain item ou bien "None"
                            pour supprimer tous les items
                            (path parameter) optionnel.
            """
            try:
                if id_item is not None:
                    # Un seul item
                    cle = ndb.Key('Item', id_item)

                    # Suppression de l'item
                    # NOTE: Si la clé n'existe pas, rien n'est supprimé
                    # mais ce n'est pas une erreur.
                    cle.delete()

                    self.response.set_status(204)
                else:
                    # Tous les items.
                    # Suppression de toutes les items.
                    ndb.delete_multi(Item.query().fetch(keys_only=True))

                    # No Content.
                    self.response.set_status(204)

            except (db.BadValueError, ValueError, KeyError):
                logging.error('%s', traceback.format_exc())
                self.error(400)

            except Exception:
                logging.error('%s', traceback.format_exc())
                self.error(500)


class CategorieHandler(webapp2.RequestHandler):

    def put(self, id_categorie=None):
        """ Permet d'ajouter ou modifier une catégorie ayant un certain id.

            Args:
                id_categorie (str): Le id de la catégorie à ajouter
                (path parameter): obligatoire.
        """
        try:
            cle = ndb.Key('Categorie', id_categorie)

            # Permet de récupérer l'entité (objet "Categorie") liée à la clé;
            # on obtient "None" si aucune entité n'est liée à cette clé.
            categorie = cle.get()

            # Y a-t-il une entité liée à la clé ?
            if categorie is None:
                # L'entité n'existe, elle sera créée (201 Created).
                status = 201

                # Création de l'entité catégorie avec une clé spécifiée et,
                # par conséquent, avec l'identifiant associé à cette clé.
                categorie = Categorie(key=cle)

                # Récupération du nom
                categorie.nom = id_categorie.strip().title()

            else:
                # L'entité existe, elle sera mise à jour (200 OK).
                # Note : On pourrait aussi utiliser 204 sans retourner une
                # représentation de la ressource.
                status = 200

                # Le corps de la requête doit contenir une expression JSON
                # avec les données de la catégorie à créer ou à modifier
                # dictionnaire entrant avant l'écriture de la bd
                categorie_dict_in = json.loads(self.request.body)

                # Récupération et formatage du nom de la catégorie; seulement
                # des majuscules pour les premières lettres de chaque mot
                # et retrait des espaces superflus.
                categorie.nom = categorie_dict_in['nom'].strip().title()

            categorie.put()

            # Configuration du code de statut HTTP (201 ou 204).
            self.response.set_status(status)

            # Création d'un dictionary contenant toutes les informations
            # sur l'entité "catégorie".
            # dictionnaire après les traitements dans la bd.
            categorie_dict_out = categorie.to_dict()

            # L'identifiant doit être ajouté manuellement car il ne fait pas
            # partie des propriétés de l'entité.
            categorie_dict_out['nom'] = categorie.key.id().strip().title()  

            # Création d'une expression JSON à partir du dictionary.
            # Il faut passer dans le paramètre "default" la fonction qui permet
            # de sérialiser les dates et heures.
            categorie_json_out = json.dumps(categorie_dict_out,
                                            default=serialiser_pour_json)

            # Configuration de l'entête HTTP "Content-Type" de la réponse.
            self.response.headers['Content-Type'] = ('application/json;' +
                                                     ' charset=utf-8')
            # Le corps de la réponse contiendra une représentation en JSON
            # de la catégorie qui vient d'être créée ou modifiée.
            self.response.out.write(categorie_json_out)

        # Exceptions en lien avec les données fournies (liées à la requête).
        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            # Bad Request.
            self.error(400)

        # Exceptions graves lors de l'exécution du code
        # (non liées à la requête).
        except Exception:
            logging.error('%s', traceback.format_exc())
            # Internal Server Error.
            self.error(500)

    def get(self, id_categorie=None, items=None):
            """ Permet d'obtenir une représentation d'une catégoie ayant un certain
                id ou bien de toutes les catégories si l'id n'est pas
                spécifié.

                Args:
                    id_item (str): l'id d'un certain item ou bien "None"
                            pour supprimer tous les items
                            (path parameter) optionnel.
                    items (str): chaine de caractères voulant qu'on veut
                            afficher tous les items de la catégorie spécifiée
            """
            try:
                print id_categorie
                # Est-ce que l'id est présent ?
                if id_categorie is not None:
                    # Une seule catégorie.
                    cle = ndb.Key('Categorie', id_categorie)

                    categorie = cle.get()
                    # Est-ce qu'il y a une catégorie liée à ce nom ?
                    if (categorie is None):
                        # Not Found.
                        self.response.set_status(404)
                        self.error(404)
                        # Fin de l'exécution.
                        return
                    # DÉBUT GET ITEM BY CAT

                    # si "items" n'es pas spécifié
                    if(items is None):
                        # Création d'un dictionnaire contenant les 
                        # valeurs de la catégorie
                        categorie_dict = categorie.to_dict()

                        # Formatage du nom pour le JSON
                        nomCat = categorie.key.id().strip().title()
                        categorie_dict['nom'] = nomCat

                        # Sérialisation pour JSON
                        json_data = json.dumps(categorie_dict,
                                               default=serialiser_pour_json)
                    else:
                        # si items == 'items'
                        if(items == 'items'):
                            # Création d'une liste d'items
                            lst_dict_item = []

                            # Requête sur les Items dans le datastore
                            requete = Item.query()
                            # Ajout d'un filtre pour spécifier les items
                            # appartenant à la catégorie dont l'id a été fourni
                            requete = requete.filter(Item.categorie == cle)

                            # Si la requête renvoie quelque chose
                            if (requete.count() > 0):
                                # Boucle sur les enregistrements de la requête
                                for item in requete.fetch():
                                    # Création d'un dictionnaire contenant les
                                    # valeurs de l'item
                                    item_dict = item.to_dict()

                                    # Formatage du nom
                                    nom_maj = item.key.id().strip().title()
                                    item_dict['nom'] = nom_maj

                                    # Formatage de la catégorie
                                    nomCat = categorie.nom.strip().lower()
                                    item_dict['categorie'] = nomCat

                                    # Ajout de l'item à la liste
                                    lst_dict_item.append(item_dict)

                                # Conversion en JSON de la liste
                                json_data = json.dumps(
                                            lst_dict_item,
                                            default=serialiser_pour_json)
                            else:
                                self.error(404)
                                self.response.write(
                                     'Aucun article dans cette catégorie')
                                self.response.write(requete)
                                return
                        else:
                            # La valeur n'est pas égale à "items"
                            # Bad Request.
                            self.error(400)
                            self.response.write("La valeur n'est pas valide")
                            return
                else:
                    # aucun id de fourni
                    # Toutes les catégories.

                    # Création d'une liste de catégories
                    lst_dict_categorie = []

                    # Création d'une requête sur le "Datastore"
                    # (toutes les catégories).
                    requete = Categorie.query()

                    # Parcours des catégories retournées par la requête.
                    for categorie in requete.fetch():

                        # Formatage du nom
                        categorie_dict = categorie.to_dict()
                        nomCat = categorie.key.id().strip().title()
                        categorie_dict['nom'] = nomCat

                        # Ajout de la catégorie à la liste
                        lst_dict_categorie.append(categorie_dict)

                    json_data = json.dumps(lst_dict_categorie,
                                           default=serialiser_pour_json)

                self.response.set_status(200)
                self.response.headers['Content-Type'] = ('application/json;' +
                                                         ' charset=utf-8')
                self.response.out.write(json_data)

            except (db.BadValueError, ValueError, KeyError):
                logging.error('%s', traceback.format_exc())
                self.error(400)

            except Exception:
                logging.error('%s', traceback.format_exc())
                self.error(500)

    def delete(self, id_categorie=None):
        """ Permet de supprimer une catégorie ayant un certain id
            ou bien toutes les catégories si l'id n'est pas spécifié.

            Args:
                id_categorie (str): L'id d'une certaine catégore ou bien "None"
                           pour supprimer toutes les catégories
                           (path parameter) optionnel.
        """
        try:

            if id_categorie is not None:
                # Une seule catégorie.
                cle = ndb.Key('Categorie', id_categorie)

                # Suppression des items de cette catégorie.
                ndb.delete_multi(Item.query(ancestor=cle).fetch
                                           (keys_only=True))
                # Suppression de la categorie.
                # NOTE: Si la clé n'existe pas, rien n'est supprimé
                # mais ce n'est pas une erreur.
                cle.delete()
                self.response.set_status(204)
            else:
                # Toutes les catégories.

                # Suppression de toutes les items
                ndb.delete_multi(Item.query().fetch(keys_only=True))
                # Suppression de toutes les catégories
                ndb.delete_multi(Categorie.query().fetch
                                                (keys_only=True))

                # No Content.
                self.response.set_status(204)

        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            self.error(400)

        except Exception:
            logging.error('%s', traceback.format_exc())
            self.error(500)


class MagasinHandler(webapp2.RequestHandler):

    def post(self, id_magasin=None, item=None):
        """ Permet d'ajouter un item dans un magasin
            Args:
                id_magasin (str): Le id du magasin dans lequel on ajoute
                l'item (path parameter): obligatoire.
        """
        try:
            # Récupération de la clé du magasin
            cle = ndb.Key('Magasin', id_magasin)

            # création d'un objet magasin avec la clé
            magasin = cle.get()

            # Si le magasin existe
            if magasin is not None:

                # Création d'un dictionnaire avec les valeurs de itemStock
                item_stock_dict = json.loads(self.request.body)

                cleStock = ndb.Key('ItemStock', id_magasin)
                itemStock = ItemStock(key=cleStock)

                # Récupération de la clé de l'item selon la valeur du body
                cleItem = ndb.Key('Item', item_stock_dict['item']
                                  .strip().lower())
                item = cleItem.get()

                # Si l'item existe
                if(item is not None):
                    itemStock.id = item.nom
                    itemStock.item = cleItem
                    itemStock.quantite = item_stock_dict['quantite']
                    itemStock.ancestor = cle
                    itemStock.parent_id_magasin = magasin.id_nom

                    status = 201
                else:
                    self.error(400)
                    return

                itemStock.put()

                self.response.set_status(status)

                item_stock_dict_out = itemStock.to_dict()

                idItemStock = cleItem.id()
                item_stock_dict_out['item'] = item.nom.strip().lower()
                item_stock_dict_out['parent_id_magasin'] = id_magasin
                # Création d'une expression JSON à partir du dictionary.
                # Il faut passer dans le paramètre "default" la fonction qui
                # permet de sérialiser les dates et heures.
                item_stock_json_out = json.dumps(item_stock_dict_out,
                                                 default=serialiser_pour_json)

                # Configuration de l'entête HTTP "Content-Type" de la réponse.
                self.response.headers['Content-Type'] = ('application/json;' +
                                                         ' charset=utf-8')
                self.response.headers['Location'] = ('/magasins/beauport/' +
                                                     'item/' + idItemStock)
                # Le corps de la réponse contiendra une représentation en JSON
                # de la catégorie qui vient d'être créée ou modifiée.
                self.response.out.write(item_stock_json_out)
            else:
                self.error(400)

        # Exceptions en lien avec les données fournies (liées à la requête).
        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            # Bad Request.
            self.error(400)

        # Exceptions graves lors de l'exécution du code
        # (non liées à la requête).
        except Exception:
            logging.error('%s', traceback.format_exc())
            # Internal Server Error.
            self.error(500)

    def put(self, id_magasin=None, nom_magasin=None,
            adresse_magasin=None, id_item=None, quantite=None):
        try:
            cle = ndb.Key("Magasin", id_magasin)
            magasin = cle.get()
            if magasin is None:
                status = 201
                print status
                magasin = Magasin(key=cle)
                magasin_dict_in = json.loads(self.request.body)
                magasin.id_nom = magasin_dict_in['id_nom'].strip().title()
                magasin.adresse = magasin_dict_in['adresse'].strip().title()
            else:
                magasin_dict_in = json.loads(self.request.body)
                magasin.id_nom = magasin_dict_in["id_nom"].strip().title()
                magasin.adresse = magasin_dict_in["adresse"]
                status = 200
                if id_item is not None:
                    cleStock = ndb.Key('ItemStock', id_magasin)
                    itemStock = cleStock.get()
                    item = itemStock.item
                    if quantite is not None:
                        item.quantite = quantite
                        self.response.set_status(200)
                    else:
                        self.response.set_status(200)

            magasin.put()

            self.response.set_status(status)

            magasin_dict_out = magasin.to_dict()

            magasin_json_out = json.dumps(magasin_dict_out,
                                          default=serialiser_pour_json)

            self.response.headers['Content-Type'] = ('application/json;' +
                                                     ' charset=utf-8')
            self.response.out.write(magasin_json_out)
        # Exceptions en lien avec les données fournies (liées à la requête).
        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            # Bad Request.
            self.error(400)

        # Exceptions graves lors de l'exécution du code
        # (non liées à la requête).
        except Exception:
            logging.error('%s', traceback.format_exc())
            # Internal Server Error.
            self.error(500)
  
    def delete(self, id_magasin=None, id_item=None, item=None):
        try:
            if id_magasin is not None:
                cle = ndb.Key('Magasin', id_magasin)
                if item is not None:
                    if id_item is None:
                        # Création d'une liste d'ItemStock
                        lst_dict_itemStock = []
                        # Requête sur les ItemStock dans le datastore
                        requete = ItemStock.query()
                        # Ajout d'un filtre pour spécifier qu'on
                        # l'item correspondant à celui entré dans l'URL
                        requete = requete.filter(ItemStock.ancestor == cle)
                        for itemStock in requete.fetch:
                            ndb.delete(itemStock.key)
                        self.response.set_status(204)
                    else:
                        cleItemStock = ndb.key('ItemStock', id_magasin)
                        itemStock = cleItemStock.get()
                        ndb.delete(itemStock.key())
                        self.response.set_status(204)
                else:
                    cle.delete()
                    self.response.set_status(204)
            else:
                ndb.delete_multi(Magasin.query().fetch(keys_only=True))
                self.response.set_status(204)

        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            self.error(400)

        except Exception:
            logging.error('%s', traceback.format_exc())
            self.error(500)

    def get(self, id_magasin=None, item=None, quantite=None):
        try:

            if id_magasin is not None:
                cle = ndb.Key('Magasin', id_magasin)
                magasin = cle.get()
                if magasin is None:
                    # Not Found.
                    self.error(404)
                    # Fin de l'exécution.
                    return
                else:
                    if item is None:
                        magasin_dict = magasin.to_dict()
                        id_nom = magasin.key.id().strip().title()
                        magasin_dict['id_nom'] = id_nom
                        magasin_dict['adresse'] = magasin.adresse
                        json_data = json.dumps(magasin_dict,
                                               default=serialiser_pour_json)
                        self.response.set_status(200)

                    # GetItemsMaxMagasin---------------------------------------------
                    else:
                        lst_dict_itemStock = []
                        requete = ItemStock.query()
                        # requete = requete.filter(ItemStock.ancestor == cle)
                        if quantite is not None:
                            quantite = self.request.get('quantite').strip().lower()
                            requete = requete.filter(ItemStock.quantite >= int(quantite))
                        
                        if (requete.count() > 0):
                            # Boucle sur les enregistrements de la requête
                            for itemStock in requete.fetch():
                                # Création d'un dictionnaire contenant les
                                # valeurs de l'item
                                itemStock_dict = itemStock.to_dict()

                                # Formatage du nom du magasin
                                nom = str(itemStock.key.id())
                                itemStock_dict['magasin'] = nom

                                # Formatage du nom de l'item
                                cleItem = itemStock.item
                                item = cleItem.get()

                                itemStock_dict['item'] = item.nom
                                # Ajout de l'item à la liste
                                lst_dict_itemStock.append(itemStock_dict)
                                
                            json_data = json.dumps(lst_dict_itemStock,
                                                   default=serialiser_pour_json)

                            if len(lst_dict_itemStock) == 0:
                                self.response.set_status(404)
                            else:
                                self.response.set_status(200)

                            self.response.headers['Content-Type'] = ('application/json;' +
                                                                     ' charset=utf-8')
                            self.response.out.write(json_data)
                        else:
                            self.error(404)
                            self.response.write(
                                'Aucun resultat')
                            return
            else:
                lst_dict_magasin = []
                requete = Magasin.query()
                for magasin in requete.fetch():
                    magasin_dict = magasin.to_dict()
                    id_nom = magasin.key.id().strip().title()
                    magasin_dict['id_nom'] = id_nom
                    magasin_dict['adresse'] = magasin.adresse
                    lst_dict_magasin.append(magasin_dict)
                json_data = json.dumps(lst_dict_magasin,
                                       default=serialiser_pour_json)
                if len(lst_dict_magasin) == 0:
                    self.response.set_status(404)
                else:
                    self.response.set_status(200)
            self.response.headers['Content-Type'] = ('application/json;' +
                                                     ' charset=utf-8')
            self.response.out.write(json_data)

        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            self.error(400)

        except Exception:
            logging.error('%s', traceback.format_exc())
            self.error(500)


class MainHandler(webapp2.RequestHandler):
    """ Classe qui permet de gérer la page d'accueil
        du site.
    """
    def get(self):
        self.response.write('Hello world!')
        self.response.set_status(200)
#
# Point d'entrée principale du site.
#
app = webapp2.WSGIApplication([

    webapp2.Route(r'/', handler=MainPageHandler,
                  methods=['GET']),

    webapp2.Route(r'/items/<id_item>', handler=ItemHandler,
                  methods=['PUT', 'GET', 'DELETE']),

    webapp2.Route(r'/items', handler=ItemHandler,
                  methods=['GET', 'DELETE']),

    webapp2.Route(r'/items/prix/<id_item>', handler=ItemHandler,
                  methods=['PUT']),

    webapp2.Route(r'/items/<id_item>/<magasins>', handler=ItemHandler,
                  methods=['GET']),

    webapp2.Route(r'/categories', handler=CategorieHandler,
                  methods=['GET', 'DELETE']),

    webapp2.Route(r'/categories/<id_categorie>', handler=CategorieHandler,
                  methods=['GET', 'PUT', 'DELETE']),

    webapp2.Route(r'/categories/<id_categorie>/<items>',
                  handler=CategorieHandler, methods=['GET']),

    webapp2.Route(r'/magasins', handler=MagasinHandler,
                  methods=['GET', 'DELETE']),

    webapp2.Route(r'/magasins/<id_magasin>', handler=MagasinHandler,
                  methods=['GET', 'PUT', 'DELETE']),

    webapp2.Route(r'/magasins/<id_magasin>/<item>', handler=MagasinHandler,
                  methods=['POST', 'GET', 'DELETE']),

    webapp2.Route(r'/magasins/<id_magasin>/item/<id_item>',
                  handler=MagasinHandler,
                  methods=['PUT', 'GET',  'DELETE']),
    webapp2.Route(r'/magasins/<id_magasin>/<id_item>',
                  handler=MagasinHandler,
                  methods=['PUT', 'GET',  'DELETE'])
], debug=True)
