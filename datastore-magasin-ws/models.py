# -*- coding: utf-8 -*-

from google.appengine.ext import ndb


class Categorie(ndb.Model):
    """
    Classe qui représente une catégorie d'item
    """

    """ @nom: Le nom de la catégorie."""
    nom = ndb.StringProperty(required=True, indexed=False)


class Item(ndb.Model):
    """
    Classe utilisée pour stocker de l'information sur
    un item
    """

    """ @nom: Le nom de l'item."""
    nom = ndb.StringProperty(required=True, indexed=True)

    """ @description: La description de l'item."""
    description = ndb.StringProperty(required=True, indexed=False)

    """ @prix: Le prix de l'item."""
    prix = ndb.FloatProperty(required=True, indexed=True)

    """ @categorie: La catégorie de l'item."""
    categorie = ndb.KeyProperty(Categorie)


class Magasin(ndb.Model):
    """
    Classe qui représente un magasin
    """

    """ @nom: Le nom du magasin."""
    id_nom = ndb.StringProperty(required=True, indexed=False)

    """ @adress: L'adresse du magasin."""
    adresse = ndb.StringProperty(required=True, indexed=False)


class ItemStock(ndb.Model):
    """
    Classe qui représente un item en stock
    """

    """ @item: L'item en soit."""
    item = ndb.KeyProperty(Item)

    """ @quantite: La quantité en stock de l'item."""
    quantite = ndb.IntegerProperty(required=True, indexed=True)
